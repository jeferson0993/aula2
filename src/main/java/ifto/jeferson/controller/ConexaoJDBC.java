package ifto.jeferson.controller;

import java.sql.Connection;

public interface ConexaoJDBC {
    public Connection criarConexao();
}
